package vip.raod.common.service;

import org.springframework.stereotype.Service;

import vip.raod.common.domain.LogDO;
import vip.raod.common.domain.PageDO;
import vip.raod.common.utils.Query;

@Service
public interface LogService {
	void save(LogDO logDO);
	PageDO<LogDO> queryList(Query query);
	int remove(Long id);
	int batchRemove(Long[] ids);
}
