package vip.raod.common.controller;

import org.springframework.stereotype.Controller;
import vip.raod.common.utils.ShiroUtils;
import vip.raod.system.domain.UserDO;

@Controller
public class BaseController {
	public UserDO getUser() {
		return ShiroUtils.getUser();
	}

	public Long getUserId() {
		return getUser().getUserId();
	}

	public String getUsername() {
		return getUser().getUsername();
	}

	public Long getDeptId() {
		return getUser().getDeptId();
	}
}